//
//  SearchPresenter.swift
//  lesson_6.7_MVP
//
//  Created by Akai on 27/3/23.
//

import UIKit

protocol SearchPresenterDelegate: AnyObject {
    init(view: SearchDelegate)
    func getBeerList()
}

class SearchPresenter: SearchPresenterDelegate {
    
    private weak var view: SearchDelegate?
//    private var service = BeerListService()
    
    required init(view: SearchDelegate) {
        self.view = view
    }
    
    func getBeerList() {
//
//        //cоздать метод в service для получения данных с сервера
//        service.getBeers { model
//            if model != nil {
//
//            } else {
//                view?.recieveError(error: Error)
//            }
//        }
    }
}
