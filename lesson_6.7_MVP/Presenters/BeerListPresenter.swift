//
//  BeerListPresenter.swift
//  lesson_6.7_MVP
//
//  Created by Akai on 27/3/23.
//

import UIKit

protocol BeerListPresenterDelegate: AnyObject {
    init(view: BeerListDelegate)
    func getBeerList()
}

class BeerListPresenter: BeerListPresenterDelegate {
    
    private weak var view: BeerListDelegate?
    private var service = BeerListService()
    
    required init(view: BeerListDelegate) {
        self.view = view
    }
    
    func getBeerList() {
        service.fetchBeer { Result<BeerElement, Error> in
            <#code#>
        }
}
