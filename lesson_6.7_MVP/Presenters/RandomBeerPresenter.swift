//
//  RandomBeerPresenter.swift
//  lesson_6.7_MVP
//
//  Created by Akai on 27/3/23.
//

import UIKit

protocol RandomBeerPresenterDelegate: AnyObject {
    init(view: RandomBeerDelegate)
    func getBeerList()
}

class RandomBeerPresenter: RandomBeerPresenterDelegate {
    
    private weak var view: RandomBeerDelegate?
//    private var service = BeerListService()
    
    required init(view: RandomBeerDelegate) {
        self.view = view
    }
    
    func getBeerList() {
//        
//        //cоздать метод в service для получения данных с сервера
//        service.getBeers { model
//            if model != nil {
//                
//            } else {
//                view?.recieveError(error: Error)
//            }
//        }
    }
}
