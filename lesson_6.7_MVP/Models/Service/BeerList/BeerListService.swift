//
//  BeerListService.swift
//  lesson_6.7_MVP
//
//  Created by Akai on 27/3/23.
//

import UIKit
import RxSwift

//enum ApiType {
//    case allBeers
//    case beer
//
//    var baseURL: URL {
//        .init(
//            string: "api.punkapi.com"
//        )!
//    }
//
//
//
//
//    var path: String {
//        switch self {
//        case .allBeers:
//            return "/beers"
//        case .beer:
//            return "/beers/1"
//        }
//    }
//
//    var fullPath: URL {
//        return URL(string: self.path, relativeTo: self.baseURL)!.absoluteURL
//    }
//
//    var components: URLComponents {
//        var components = URLComponents()
//        components.scheme = "https"
//        components.host = "www.thecocktaildb.com"
//        switch self {
//        case .allBeers:
//            components.path = "/v2\(ApiType.allBeers.path)"
//        case .beer:
//            components.path = "/v2\(ApiType.beer.path)"
//        }
//        return components
//    }
//}



final class BeerListService {
    
    
    
    static let shared = BeerListService()
    
//    private let session = URLSession(configuration: .default)
    
    private var baseURL = URL(string: "https://api.punkapi.com/v2/beers")
    private let session = URLSession(configuration: .default)
    private var dataTask: URLSessionDataTask? = nil
    
    private init () { }
    
    
    func decoder<T: Decodable> (data: Data) throws -> T {
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    }
    
    
    func callAPI() -> Observable<[BeerElement]> {
        return Observable<[BeerElement]>.create { observer in
            self.dataTask = self.session.dataTask(with: self.baseURL!) { data, response, error in
                do {
                    let model = try JSONDecoder().decode([BeerElement].self, from: data!)
                    observer.onNext(model.self)
                    print(model)
                } catch let error {
                    observer.onError(error)
                    print(error.localizedDescription)
                }
                observer.onCompleted()
            }
            self.dataTask?.resume()
            self.dataTask?.cancel()
            return Disposables.create()
    }
    }

    func fetchBeer(completion: @escaping (Result<BeerElement, Error>) -> Void) {
        guard let request = baseURL else { return }
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else { return }
            let decoder = JSONDecoder()
            do {
                let decodedData = try decoder.decode(BeerElement.self, from: data)
                completion(.success(decodedData))
                print(decodedData)
            } catch {
                completion(.failure(error))
                print(error.localizedDescription)
            }
        }
        task.resume()
    }

//    func decodeOrderTypeData(_ json: String) -> [OrderTypeModel] {
//        var orderTypeModelArray = [OrderTypeModel]()
//        let orderTypeData = Data(json.utf8)
//        let jsonDecoder = JSONDecoder()
//        do { let orderTypeModelData = try jsonDecoder.decode([OrderTypeModel].self, from: orderTypeData)
//            orderTypeModelArray = orderTypeModelData
//        } catch {
//            print(error.localizedDescription)
//        }
//        return orderTypeModelArray
//    }
}


