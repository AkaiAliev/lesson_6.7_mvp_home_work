//
//  RandomBeerBuilder.swift
//  lesson_6.7_MVP
//
//  Created by Akai on 27/3/23.
//

import UIKit

class RandomBeerBuilder {
    static func build() -> UIViewController {
        let vc = RandomBeerVC()
        let presenter = RandomBeerPresenter(view: vc)
        vc.presenter = presenter
        return vc
    }
}

