//
//  RandomBeerVC.swift
//  lesson_6.7_MVP
//
//  Created by Akai on 27/3/23.
//

import UIKit

protocol RandomBeerDelegate: AnyObject {

}

final class RandomBeerVC: UIViewController {
    
    var presenter: RandomBeerPresenterDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemPink
    }
}

extension RandomBeerVC: RandomBeerDelegate {
    
}
