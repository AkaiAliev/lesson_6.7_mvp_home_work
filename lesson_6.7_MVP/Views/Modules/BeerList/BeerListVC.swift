//
//  BeerListVC.swift
//  lesson_6.7_MVP
//
//  Created by Akai on 27/3/23.
//

import UIKit

protocol BeerListDelegate: AnyObject {
    func recieveBeer(beers: [BeerElement])
    func recieveError(error: Error)
}

final class BeerListVC: UIViewController {
    
    var presenter: BeerListPresenterDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemPurple
        presenter.getBeerList()
        
    }
}

extension BeerListVC: BeerListDelegate {
    func recieveError(error: Error) {
        print(error.localizedDescription)
    }
    
    func recieveBeer(beers: [BeerElement]) {
        print(beers)
    }
    
    
}
