//
//  SearchBuilder.swift
//  lesson_6.7_MVP
//
//  Created by Akai on 27/3/23.
//

import UIKit

class SearchBuilder {
    static func build() -> UIViewController {
        let vc = SearchVC()
        let presenter = SearchPresenter(view: vc)
        vc.presenter = presenter
        return vc
    }
}
