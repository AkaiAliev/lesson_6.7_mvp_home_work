//
//  MainTabBarController.swift
//  lesson_6.7_MVP
//
//  Created by Akai on 27/3/23.
//

import UIKit

class MainTabBarController: UITabBarController, UITabBarControllerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        configureTabBarItems()
    }
    
    private func configureTabBarItems() {
        let beerListVC = BeerListBuilder.build()
        beerListVC.tabBarItem = UITabBarItem(title: "Beer List", image: UIImage(systemName: "1.circle"), tag: 0)
        
        let randomBeerVC = RandomBeerBuilder.build()
        randomBeerVC.tabBarItem = UITabBarItem(title: "Random Beer", image: UIImage(systemName: "2.circle"), tag: 1)
        
        let searchVC = SearchVC()
        searchVC.tabBarItem = UITabBarItem(title: "Search Bar", image: UIImage(systemName: "3.circle"), tag: 2)
        
        let listNavigationVC = UINavigationController(rootViewController: beerListVC)
        let randomNavigationVC = UINavigationController(rootViewController: randomBeerVC)
        let searchNavigationVC = UINavigationController(rootViewController: searchVC)
        
        setViewControllers([listNavigationVC, randomNavigationVC, searchNavigationVC], animated: true)
    }
}
